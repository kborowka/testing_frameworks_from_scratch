
def sum_of_numbers(a, b):
    a = int(a)
    b = int(b)
    if a < 0:
        print("{} is negative")
    return a + b


if __name__ == "__main__":
    num1 = input()
    num2 = input()
    print(sum_of_numbers(num1, num2))

